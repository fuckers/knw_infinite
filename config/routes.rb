KnowInfinite::Application.routes.draw do


  #get "sessions/login"
  #get "sessions/home"
  #get "sessions/profile"
  #get "sessions/,"
  #get "sessions/setting"
  #get "users/new"
  
  #match ':controller(/:action(/:id))(.:format)' , :via=> :get

  root :to => "sessions#login"
  match "courses/browse", :to=> "courses#browse", :via=>:get
  match "signup", :to => "users#new", :via=>:get
  post "users/create"
  match "login", :to => "sessions#login", :via=>:get
  match "logout", :to => "sessions#logout", :via=>:get
  match "home", :to => "sessions#home", :via=>:get
  match "profile", :to => "sessions#profile", :via=>:get
  match "setting", :to => "sessions#setting", :via=>:get
  match "login_attempt", :to => "sessions#login_attempt", :via=>:post
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay outwith "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  resources :nuggets

  resources :courses do
    member do
      get 'view'
    end
  end

  match ':controller(/:action(/:id))', :via => :get
  match ':controller(/:action(/:id))', :via => :post
end
