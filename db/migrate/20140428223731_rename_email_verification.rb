class RenameEmailVerification < ActiveRecord::Migration
  def up
  	rename_column("users","email_verification_token","verified")
  end

  def down
  	rename_column("users","verified","email_verification_token")
  end

end
