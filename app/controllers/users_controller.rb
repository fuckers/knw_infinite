class UsersController < ApplicationController  
  before_filter :save_login_state, :only => [:new, :create]
  def new
    @user = User.new 
  end
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = "Thanks for signing up, we've delivered an email to you with instructions on how to complete your registration!"
      flash[:color]= "valid"
      @user.deliver_verification_instructions!
      redirect_to root_url      
    else
      flash[:notice] = "Form is invalid"
      flash[:color]= "invalid"
    end
    render "new"
  end

  private 
    def user_params
      params.require(:user).permit(:username, :email, :password, :password_confirmation)
    end
end