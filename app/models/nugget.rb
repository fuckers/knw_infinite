class Nugget < ActiveRecord::Base
	belongs_to :course
	acts_as_list :scope => :course

	#named scopes
	scope :sorted, lambda { order("nuggets.position ASC") }
end
