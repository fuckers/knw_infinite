class AlterUsers < ActiveRecord::Migration
  
  def up
  	add_column("users","email_verification_token",:boolean)
  	add_column("users","date_of_birth",:date)
  	add_column("users","country",:string)
  	add_column("users","city",:string)
  	add_column("users","address",:string)
  	add_column("users","email_verification_link",:string)
  end

  def down
  	remove_column("users","email_verification_token")
  	remove_column("users","date_of_birth")
  	remove_column("users","country")
  	remove_column("users","city")
  	remove_column("users","address")
	remove_column("users","email_verification_link")  
  end

end
