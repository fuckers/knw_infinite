class NuggetsController < ApplicationController
  before_action :confirm_logged_in
  before_action :set_nugget, only: [:show, :edit, :update, :destroy]
  before_action :set_course, only: [:index, :new, :edit, :update, :delete, :create]

  # GET /nuggets
  # GET /nuggets.json
  def index
    @nuggets = @course.nuggets.sorted
  end

  # GET /nuggets/1
  # GET /nuggets/1.json
  def show
  end

  # GET /nuggets/new
  def new
    @nugget = Nugget.new
  end

  # GET /nuggets/1/edit
  def edit
  end

  # POST /nuggets
  # POST /nuggets.json
  def create
    @nugget = Nugget.new(nugget_params)
    @nugget.course_id = @course.id
    @nugget.user_id = session[:user_id]

    respond_to do |format|
      if @nugget.save
        format.html { redirect_to @nugget, notice: 'Nugget was successfully created.' }
        format.json { render action: 'show', status: :created, location: @nugget }
      else
        format.html { render action: 'new' }
        format.json { render json: @nugget.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nuggets/1
  # PATCH/PUT /nuggets/1.json
  def update
    respond_to do |format|
      if @nugget.update(nugget_params)
        format.html { redirect_to @nugget, notice: 'Nugget was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @nugget.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nuggets/1
  # DELETE /nuggets/1.json
  def destroy
    @nugget.destroy
    respond_to do |format|
      format.html { redirect_to nuggets_url(:course_id => @nugget.course_id)}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nugget
      @nugget = Nugget.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nugget_params
      params.require(:nugget).permit(:name, :description, :course_id, :user_id, :position)
    end

    def set_course
      if params[:course_id]
        @course = Course.find(params[:course_id])
      elsif params[:nugget][:course_id]
        @course = Course.find(params[:nugget][:course_id])
      else
        puts "Could not find course_id"
      end
    end
end
