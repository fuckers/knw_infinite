class Course < ActiveRecord::Base
	belongs_to :user, :foreign_key => "user_id"
	has_many :nuggets

	#named scopes
	scope :latest_updated, lambda { order("courses.updated_at DESC")}
end