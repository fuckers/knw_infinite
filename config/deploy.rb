set :application, "know_infinite"
set :repository,  "https://mikevictor21@bitbucket.org/fuckers/knw_infinite.git"
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "192.241.188.227"                 
role :app, "192.241.188.227"                        
role :db,  "192.241.188.227", :primary => true

set :user, "rails"
set :password, "3Uv5oJUg2T"

set :use_sudo, false

set :deploy_to, "/home/rails/"

set :deploy_via, :copy

set :normalize_asset_timestamps, false

require 'bundler/capistrano'
require "rvm/capistrano"

set :rvm_type, :system

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end