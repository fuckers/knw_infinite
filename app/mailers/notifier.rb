class Notifier < ActionMailer::Base
  default_url_options[:host] = "www.mandrillapp.com"

  def verification_instructions(user)
  	subject       "Email Verification"
  	from          "no-reply@mandrillapp.com"
  	recipients    user.email
  	sent_on       Time.now
  	body          :verification_url => user_verification_url(user.perishable_token)
  end

end
